# GPU programming with CUDA

The present project contains exercises using CUDA C and PyCUDA for GPU programming using C and Python respectively.

In particular, the Python codes are presented without any print statement for compatibility purposes, so that they can be directly executed in an interactive session or inside a Jupyter notebook. 

## Exercises

 - [Vector addition](vector_add) Simple vector addition. Level: **basic**

