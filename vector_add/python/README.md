# Vector addition with PyCUDA

This exercise is intended to implement the vector addition using three different models (GPUarrays, Elementwise kernels, SourceModule function).

The goal is to see the different possibilities of implementation and be able to compare them in terms of simplicity and flexibility.

